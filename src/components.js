const createToggleButton = label => {
    let container = $(`<div>`);
    let labelText = $(`<p>${label}</p>`);
    let button = $(`<button>▶</button>`);
    let clicked = button.asEventStream("click");
    let state = clicked.scan(false, a => !a);

    const toggleButtonImage = toggled =>
	  toggled ? button.text("⏸") : button.text("▶");

    state.onValue(toggleButtonImage);

    labelText.appendTo(container);
    button.appendTo(container);
    return {
	element: container,
	state: state,
	eventStream: clicked,
	toggle: setEnabled(button)
    };
};

const createTextInputElement = (label, placeHolder="") => {
    let element = $(`<div>`);
    let labelText = $(`<p>${label}</p>`);
    let inputElement = $(`<input type="text" name="${label}" placeholder="${placeHolder}"/>`);
    let state = Bacon.UI.textFieldValue(inputElement);
    element.append(labelText);
    element.append(inputElement);
    return {
	name: label,
	element,
	state,
	toggle: setEnabled(inputElement)
    };
};

const createSimpleButtonElement = (label, buttonText) => {
    let element = $('<div>');
    let labelText = $(`<p>${label}</p>`);
    let buttonElement = $(`<button>${buttonText}</button>`);
    let clicked = buttonElement.asEventStream('click');
    element.append(labelText);
    element.append(buttonElement);
    return {
	name: label,
	element,
	buttonElement,
	clicked,
	toggle: setEnabled(buttonElement)
    };
};

const createCounterElement = (label) => {
    let element = $('<div>');
    let labelText = $(`<p>${label}</p>`);
    let upButton = $('<button>+</button>');
    let downButton = $('<button>-</button>');
    let counter = $(`<span></span>`);
    let clickUp = upButton.asEventStream('click');
    let clickDown = downButton.asEventStream('click');
    element.append(labelText);
    element.append(upButton);
    element.append(downButton);
    element.append(counter);
    return {
	element,
	clickUp,
	clickDown,
	counter,
	toggle: toggled => {
	    setEnabled(upButton, toggled);
	    setEnabled(downButton, toggled);
	}
    };
};

const createTextOutputField = (label, content = "") => {
    let element = $('<div>');
    let labelText = $(`<p>${label}</p>`);
    let bodyContent = $(`<p>${content}</p>`);
    element.append(labelText);
    element.append(bodyContent);
    return {
	element,
	content: bodyContent
    };
};
